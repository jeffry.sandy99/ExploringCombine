//
//  ViewController.swift
//  ExploringCombine
//
//  Created by Jeffry Sandy Purnomo on 03/04/21.
//

import UIKit
import Combine

class MyCustomTableCell: UITableViewCell{
    
    let action = PassthroughSubject<String, Never>()
    
    private let button: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .systemPink
        btn.setTitle("Button", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        button.frame = CGRect(x: 10, y: 3, width: contentView.frame.width - 20, height: contentView.frame.height - 6)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        contentView.addSubview(button)
    }
    
    @objc private func didTapButton(){
        action.send("Cool Button was Tapped")
    }
    
}

class ViewController: UIViewController, UITableViewDataSource {

    private let tableView: UITableView = {
        let table = UITableView()
        
        table.register(MyCustomTableCell.self, forCellReuseIdentifier: "cell")
        
        return table
    }()
    
    var observers = [AnyCancellable]()
    private var models = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.frame = view.bounds
        
        APICaller.shared.fetchCompanies()
            .receive(on: DispatchQueue.main)
            .sink(
            receiveCompletion: { completion in
                switch completion{
                case .failure(let error):
                    print(error)
                case .finished:
                    print("Finished")
                }
                
        }, receiveValue: { [weak self] value in
            self?.models = value
            self?.tableView.reloadData()
        })
            .store(in: &observers)
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MyCustomTableCell else {
            fatalError()
        }
        cell.action.sink { string in
            print(string)
        }.store(in: &observers)
//        cell.textLabel?.text = models[indexPath.row]
        return cell
    }
}

