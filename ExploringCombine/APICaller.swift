//
//  APICaller.swift
//  ExploringCombine
//
//  Created by Jeffry Sandy Purnomo on 03/04/21.
//

import Combine
import Foundation

class APICaller {
    static let shared = APICaller()
    
    //    func fetchData(completion: ([String]) -> Void){
    //        completion(["Apple"])
    //    }
    
    func fetchCompanies() -> Future<[String], Error>{
        return Future { promixe in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                promixe(.success(["Apple", "Google", "Microsoft", "Facebook"]))
            }
        }
    }
}
